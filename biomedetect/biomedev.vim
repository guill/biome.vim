"See if we're developing biome.vim right now
function! s:IsBiomeDev()
	return filereadable("autoload/biome.vim")
endfunction
call biome#RegisterBiome("biomedev", function("s:IsBiomeDev"), [], [])
