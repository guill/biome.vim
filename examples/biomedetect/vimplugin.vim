"This is a vim plugin
function! s:IsVimPlugin()
	if !isdirectory("plugin") && !isdirectory("autoload")
		return 0
	endif
	return globpath("plugin","**/*.vim")!="" || globpath("autoload","**/*.vim")!=""
endfunction
call biome#RegisterBiome("vimplugin", function("s:IsVimPlugin"), [], [])
