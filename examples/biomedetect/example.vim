function! s:TestFunc()
	return 0
endfunction

call biome#RegisterBiome("test1", function("s:TestFunc"), [], [])

"The third argument is a list of biomes we want to be loaded after. This lets
"us further specialize or override their settings. If one of these biomes
"isn't registered, we'll still load just fine.
call biome#RegisterBiome("test2", function("s:TestFunc"), ["test1"], [])

"You don't need to worry about what order you register biomes in.
call biome#RegisterBiome("test3", function("s:TestFunc"), ["test2"], [])
call biome#RegisterBiome("test4", function("s:TestFunc"), ["test3"], [])

"The fourth argument is a list of biomes to shadow. If this biome is loaded,
"we won't load the shadowed biome (or any biomes it shadows transitively)
call biome#RegisterBiome("test5", function("s:TestFunc"), ["test4"], ["test4"])

"You generally shouldn't call any biome function except biome#RegisterBiome in
"the body of a biomedetect file. Feel free to call biome#IsInBiome within
"filter test functions though.

