"Put any vimplugin specific commands here
echom 'In "vimplugin" biome'

function! s:EchoStr(str)
	echo eval("a:str")
endfunction

command! DisplayBiomes call biome#DoForActive(function("s:EchoStr"))
command! ReloadBiomes call biome#RecalculateBiomes(1) | call biome#DoForActive(function("biome#LoadBiomePlugin"))

