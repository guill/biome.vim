let s:biome_info={}

let s:cached_biome_order=[]
let s:cached_biome_enabled={}
let s:cached_biome_shadowed={}

function! biome#Load()
endfunction

function! biome#RegisterBiome(name, TestFunc, process_after, shadows)
	let s:biome_info[a:name]={"name": a:name, "Test": a:TestFunc, "process_after": a:process_after, "shadows": a:shadows}
endfunction

function! biome#IsInBiome(name)
	if a:name==#"any"
		return !s:IsShadowed(a:name)
	elseif a:name==#"none"
		return empty(s:cached_biome_enabled)
	else
		return has_key(s:cached_biome_enabled, a:name) && !s:IsShadowed(a:name)
	endif
endfunction

function! biome#RecalculateBiomes(loadRules)
	if a:loadRules
		"Let everyone register detection
		let s:biome_info={}
		runtime! biomedetect/**/*.vim
	endif

	let s:cached_biome_order=[]
	let s:cached_biome_enabled={}
	let s:cached_biome_shadowed={}

	call s:SortBiomes()

	for biome_name in s:cached_biome_order
		let l:biome=s:biome_info[biome_name]
		if l:biome.Test()
			let s:cached_biome_enabled[biome_name]=1
			for shadowName in l:biome.shadows
				call s:Shadow(shadowName)
			endfor
		endif
	endfor
endfunction

function! biome#LoadBiomePlugin(biome_name)
	exec "runtime! biomeplugin/" . a:biome_name . ".vim"
	exec "runtime! biomeplugin/" . a:biome_name . "/**.vim"
endfunction

function! biome#LoadBiomePluginAfter(biome_name)
	exec "runtime! biomeafter/" . a:biome_name . ".vim"
	exec "runtime! biomeafter/" . a:biome_name . "/**.vim"
endfunction

function! biome#DoForActive(Func)
	if biome#IsInBiome("any")
		call a:Func("any")
	endif

	if biome#IsInBiome("none")
		call a:Func("none")
	endif

	for biome_name in s:cached_biome_order
		if biome#IsInBiome(biome_name)
			call a:Func(biome_name)
		endif
	endfor
endfunction

function! s:IsShadowed(name)
	return get(s:cached_biome_shadowed, a:name, 0)
endfunction

function! s:Shadow(name)
	if get(s:cached_biome_shadowed, a:name, 0)
		return
	endif
	let s:cached_biome_shadowed[a:name]=1
	if !has_key(s:biome_info,a:name)
		return
	endif
	let l:biomeInfo=s:biome_info[a:name]
	for shadow_name in l:biomeInfo.shadows
		call s:Shadow(shadow_name)
	endfor
endfunction

function! s:SortBiomes()
	"Count dependencies
	let l:num_dependencies={}
	let l:process_before={}
	for biome_name in keys(s:biome_info)
		let l:biomeInfo=s:biome_info[biome_name]
		for dependency in l:biomeInfo.process_after
			if has_key(s:biome_info, dependency)
				let l:num_dependencies[biome_name]=get(l:num_dependencies, biome_name, 0) + 1
				let l:process_before[dependency]=get(l:process_before, dependency, [])
				call add(l:process_before[dependency], biome_name)
			endif
		endfor
	endfor

	"Find dependency-less entries
	let l:available=[]
	for biome_name in keys(s:biome_info)
		if get(l:num_dependencies, biome_name, 0) == 0
			call add(l:available, biome_name)
		endif
	endfor

	"Topological sort
	let l:biome_order=[]
	while !empty(l:available)
		let l:name=l:available[0]
		let l:available=l:available[1:]
		call add(l:biome_order, l:name)
		for child in get(l:process_before,l:name,[])
			let l:num_dependencies[child]=l:num_dependencies[child]-1
			if l:num_dependencies[child] == 0
				unlet l:num_dependencies[child]
				call add(l:available, child)
			endif
		endfor
	endwhile

	if !empty(l:num_dependencies)
		echoerr "Biome dependency loop"
	endif

	let s:cached_biome_order=l:biome_order
endfunction

"Add a debugging command to view active biomes
function! s:EchoStr(str)
	echo eval("a:str")
endfunction
command! BiomeDisplayActive call biome#DoForActive(function("s:EchoStr"))

"Evaluate what biomes we're in
call biome#RecalculateBiomes(1)

"Load the relevant plugins
call biome#DoForActive(function("biome#LoadBiomePlugin"))
